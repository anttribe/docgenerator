package com.anttribe.docgenerator.samples.resume.model.type;

/**
 * @author zhaoyong
 * @date 2021/11/25 0025
 */
public enum JobStatus {

    /** 离职 */
    BETWEENJOBS;

}
