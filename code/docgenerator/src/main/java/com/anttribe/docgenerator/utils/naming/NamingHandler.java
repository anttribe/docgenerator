package com.anttribe.docgenerator.utils.naming;

/**
 * @author zhaoyong
 * @date 2021/10/6 0006
 */
public interface NamingHandler {

    /**
     * 命名
     *
     * @return
     */
    String naming();

}
